# webapi-boilerplate

You can check out **docker-compose.yaml** order to set up environment for the application.


# In the development

```bash
$ pip install -e ".[dev,test]"
$ ruff check . --fix            # for linting and fix if need
$ mypy .                        # for type checking
$ pytest                        # for unit testing
```

# In the production

```bash
$ myapp web   # starting web application
$ myapp shell # starting new shell, which live in the application context
```
