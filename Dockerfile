FROM python:3.11.3

WORKDIR /app

ADD . .

RUN pip install -e .

ENTRYPOINT ["myapp", "web"]
