import pytest
from unittest import mock

from fastapi.testclient import TestClient


TEST_DATA = [
    (
        {"id": 100, "avatar_url": "", "name": "", "login": ""},
        {"status": 200},
    ),
]


@pytest.mark.parametrize("github_data,expect", TEST_DATA)
def test_get_user(github_data: dict, expect: dict, web_app: TestClient):
    with mock.patch("app.services.github.get_user_by_username") as patcher:
        patcher.return_value = github_data
        response = web_app.get("/v1/github/users/magiskboy")
        assert response.status_code == expect.get("status")
