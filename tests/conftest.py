import pytest
from fastapi.testclient import TestClient

from app import web


@pytest.fixture(scope="session")
def web_app() -> TestClient:
    app = web.create_app()
    return TestClient(app)
