import functools
import logging
import enum

from pydantic import (
    BaseSettings,
    Field,
    RedisDsn,
)


__version__ = "1.0.0"


class LogLevel(enum.IntEnum):
    debug = logging.DEBUG
    info = logging.INFO
    warn = logging.WARN
    error = logging.ERROR
    critical = logging.CRITICAL


class Config(BaseSettings):
    LOG_LEVEL: LogLevel = Field(LogLevel.info)

    VERSION: str = Field("1.0.0")

    NAME: str = Field("App name")

    SQLALCHEMY_URI: str | None

    REDIS_URI: RedisDsn | None

    class Config:
        env_prefix = "APP_"


@functools.lru_cache()
def get_config():
    return Config()
