from pydantic import BaseModel


class GithubUser(BaseModel):
    id: int
    avatar_url: str
    name: str
    login: str