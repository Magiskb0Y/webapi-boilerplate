from contextlib import asynccontextmanager

from fastapi import FastAPI

from app import config

from . import github_user_v1


@asynccontextmanager
async def lifespan(app: FastAPI):
    # on startup
    yield
    # on shutdown


def create_app() -> FastAPI:
    cnf = config.get_config()

    app = FastAPI(title=cnf.NAME, version=cnf.VERSION, lifespan=lifespan)

    app.include_router(github_user_v1.router, prefix="/v1/github", tags=["Github"])

    return app
