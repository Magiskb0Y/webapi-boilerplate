from fastapi import APIRouter

from app.services import github

from .dtos import github_user_v1_dtos

router = APIRouter()


@router.get("/users/{username}", response_model=github_user_v1_dtos.GithubUser)
async def get_user_by_username(username: str):
    user_data = await github.get_user_by_username(username)
    return github_user_v1_dtos.GithubUser.parse_obj(user_data)