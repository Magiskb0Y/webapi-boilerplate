import urllib.parse

def url_encode(string) -> str:
    return urllib.parse.quote(string, safe="")
