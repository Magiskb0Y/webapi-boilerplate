from contextlib import contextmanager
from functools import lru_cache
from typing import Any

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base, sessionmaker

from app import config

Base: Any = declarative_base()


@lru_cache()
def get_session_klass() -> sessionmaker[Session]:
    cnf = config.get_config()
    engine = create_engine(cnf.SQLALCHEMY_URI, pool_recycle=3600)

    return sessionmaker(autocommit=False, autoflush=False, bind=engine)


@contextmanager
def db_session():
    SessionLocal = get_session_klass()
    with SessionLocal() as session:
        yield session
