import sys
import os
import code
import readline
from rlcompleter import Completer
import asyncio

import typer
import uvicorn
from fastapi import testclient

from app import web, config

app = typer.Typer(no_args_is_help=True)
web_app = web.create_app()


@app.command("web", help="Start webserver")
def start_web(
    host: str = typer.Option("127.0.0.1", help="Host for serving"),
    port: int = typer.Option(5000, help="Port for serving"),
    reload: bool = typer.Option(
        False,
        help="Reload if source code changed, number of workers is single automatically",
    ),
    log_level: str = typer.Option("info", help="Log level"),
    access_log: bool = typer.Option(False, help="Access log"),
):
    """Start an application server.

    This server is for both development and production purposes only. It does provide
    the stability, security, or performance of production WSGI servers.
    """
    cnf = config.get_config()
    app_config = uvicorn.Config(
        app="app.cli:web_app",
        host=host,
        port=port,
        log_level=log_level or cnf.LOG_LEVEL,
        access_log=access_log,
        workers=1 if reload else None,
    )
    server = uvicorn.Server(app_config)
    if reload:
        uvicorn.supervisors.ChangeReload(
            app_config, target=server.run, sockets=[app_config.bind_socket()]
        ).run()
    else:
        server.run()


@app.command("shell")
def start_shell():
    """Run an interactive Python shell in the context of a given
    FastAPI application. The application will populate the default
    namespace of this shell according to its configuration.

    This is useful for executing small snippets of management code
    without having to manually configure the application.
    """

    cnf = config.get_config()
    app = web.app.create_app()
    client = testclient.TestClient(app)
    banner = f"Python {sys.version} on {sys.platform}\n"
    banner += f"Workflow manager version {cnf.VERSION}"
    ctx = {"app": app, "client": client, "asyncio": asyncio}

    startup = os.environ.get("PYTHONSTARTUP")
    if startup and os.path.isfile(startup):
        with open(startup) as f:
            eval(compile(f.read(), startup, "exec"), ctx)

    # Site, customize, or startup script can set a hook to call when
    # entering interactive mode. The default one sets up readline with
    # tab and history completion.
    interactive_hook = getattr(sys, "__interactivehook__", None)

    if interactive_hook is not None:
        readline.set_completer(Completer(ctx).complete)
        interactive_hook()

    code.interact(banner=banner, local=ctx)
