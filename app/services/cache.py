import hashlib
import inspect
import json
from typing import Any, Optional, Union, Callable
from datetime import timedelta
import functools

import redis

from app import config


from . import logging

_logger = logging.get_logger("services.cache")


@functools.lru_cache()
def _get_redis() -> redis.Redis:
    cnf = config.get_config()
    return redis.Redis.from_url(cnf.REDIS_URI, db=1)


def _default_cache_key_func(func, *args, **kwargs):
    args_str = ""
    if args:
        args_str = ",".join(map(str, args))

    kwargs_str = ""
    if kwargs:
        kwargs_str = ",".join(["{k}={v}" for k, v in kwargs.items()])

    data = f"{func.__name__}({args_str},{kwargs_str})"
    key = hashlib.md5(data.encode("utf-8")).hexdigest()
    return key


def get_cache(key: str) -> Optional[Any]:
    try:
        data = _get_redis().get(key)
    except Exception as e:
        _logger.error(e, exc_info=True)
        return None

    if not data:
        return None

    try:
        return json.loads(data)
    except json.decoder.JSONDecodeError as e:
        _logger.error(e, exc_info=True)
        return None


def set_cache(key: str, data: Any, ttl: Optional[Union[timedelta, int]] = None):
    if not ttl:
        ttl = timedelta(minutes=10)
    try:
        serialized_data = json.dumps(data)
        is_success = _get_redis().set(key, serialized_data, ex=ttl)
        if is_success:
            _get_redis().expire(key, ttl)
    except Exception as e:
        _logger.error(e, exc_info=True)
        return False


def cached(
    ttl: Optional[Union[timedelta, int]] = None,
    key_func: Optional[Callable[[Any], str]] = None,
) -> Callable:
    def decorate(func):
        if inspect.iscoroutinefunction(func):

            async def wrapper(*args, **kwargs):
                if key_func:
                    key = key_func(*args, **kwargs)
                else:
                    key = _default_cache_key_func(func, *args, **kwargs)
                cache_data = get_cache(key)
                if cache_data:
                    return cache_data

                if not cache_data:
                    ret = await func(*args, **kwargs)
                    if ret:
                        set_cache(key, ret, ttl)
                    return ret

                return ret

        else:

            def wrapper(*args, **kwargs):
                if key_func:
                    key = key_func(*args, **kwargs)
                else:
                    key = _default_cache_key_func(func, *args, **kwargs)
                cache_data = get_cache(key)
                if cache_data:
                    return cache_data

                if not cache_data:
                    ret = func(*args, **kwargs)
                    if ret:
                        set_cache(key, ret, ttl)
                    return ret

                return ret

        return wrapper

    return decorate
