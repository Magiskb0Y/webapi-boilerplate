import httpx
import json
from datetime import timedelta

from app import helpers

from . import cache


async def _call_api(
    url: str,
    method: str | None = "GET",
    data: dict | None = None,
    params: dict[str, str | int | float | bool] | None = None,
    headers: dict[str, str] | None = None,
) -> dict | str:
    base_url = "https://api.github.com"
    method = method or "GET"
    async with httpx.AsyncClient(base_url=base_url) as client:
        response = await client.request(
            method, url, headers=headers, params=params, json=data
        )
        response.raise_for_status()
        try:
            return response.json()
        except json.JSONDecodeError:
            return response.text


@cache.cached(ttl=timedelta(days=1))
async def get_user_by_username(username: str) -> dict:
    return await _call_api(f"/users/{helpers.url_encode(username)}") # type: ignore
