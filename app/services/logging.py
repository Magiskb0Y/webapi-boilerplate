import logging

from app import config


def get_logger(name: str):
    cnf = config.get_config()
    loglevel = cnf.LOG_LEVEL
    logger = logging.getLogger(name)
    logger.setLevel(loglevel)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(levelname)s:     %(msg)s"))
    logger.addHandler(handler)

    return logger
